/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import BaseDatos.BDConexion;
import Datos.Producto;
import Interfaz.Convertidor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vi_ma
 */
public class ControlProducto {
    
    public void agregarProducto(Producto mProducto) throws SQLException {
        String SQL = "insert into producto values "
                + "(null,'?1','?2','?3','?4','?5')";
        SQL = SQL.replace("?1", mProducto.getNombre());
        SQL = SQL.replace("?2", mProducto.getDescripcion());
        SQL = SQL.replace("?3", String.valueOf(mProducto.getPrecio()));
        SQL = SQL.replace("?4", mProducto.getImagen());
        SQL = SQL.replace("?5", String.valueOf(mProducto.getVisible()));

        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);

    }

    public void eliminarProducto(String id) throws SQLException {
       String SQL = "UPDATE producto set visible = 0 where idproducto = \"" + id + "\"";
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);
    }

    public void modificarProducto(Producto mProducto,String row) throws SQLException {
        String SQL = "update producto set "
                + "nombre='?1',"
                + "descripcion='?2',"
                + "precio='?3',"
                + "imagen = '?4',"
                + "visible = '?5'"
                + " where idproducto = \"" + row + "\"";
        SQL = SQL.replace("?1", mProducto.getNombre());
        SQL = SQL.replace("?2", mProducto.getDescripcion());
        SQL = SQL.replace("?3", String.valueOf(mProducto.getPrecio()));
        SQL = SQL.replace("?4", mProducto.getImagen());
        SQL = SQL.replace("?5", String.valueOf(mProducto.getVisible()));
        
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);

    }
    
       
    
    public ResultSet ObtenerUrl(String row) throws SQLException{
          String SQL = "select producto.imagen from producto where idproducto = \"" + row + "\"";
            ResultSet ListaProductos;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaProductos = mConexion.ejecutarConsulta(SQL);
        return ListaProductos;
    }
    
     
    
    public ResultSet consultarTodasProductos() {
        String SQL = "select producto.idproducto, producto.imagen,producto.nombre,producto.descripcion,producto.precio"
                + " from producto where visible = 1";
        
        ResultSet ListaProductos;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaProductos = mConexion.ejecutarConsulta(SQL);

        return ListaProductos;
    }
    
}

