/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import BaseDatos.BDConexion;
import Datos.DetallesCompra;
import Datos.DetallesVenta;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Ryogan
 */
public class ControlDetallesCompra {
    public void agregarDetallesCompra(DetallesCompra mDetallesCompra) throws SQLException {
        String SQL = "insert into detallecompra values"
                + "(null,'?2','?3','?4',(select max(idcompra) from compra));";
       
        SQL = SQL.replace("?2", String.valueOf(mDetallesCompra.getprecio()));
        SQL = SQL.replace("?3", String.valueOf(mDetallesCompra.getcantidad()));
        SQL = SQL.replace("?4", String.valueOf(mDetallesCompra.getNombre()));
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);

    }
    
    public ResultSet consultarTodasCompras(String Inicio,String Final) {
        String SQL = "select idcompra,fecha,total from compra "
                + "where fecha between '?1' and '?2'";
        SQL = SQL.replace("?1", Inicio);
        SQL = SQL.replace("?2", Final);
        ResultSet ListaDetallesVentas;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaDetallesVentas = mConexion.ejecutarConsulta(SQL);

        return ListaDetallesVentas;
    }
    
   public ResultSet consultarTodasDetallesCompras(String Inicio,String Final, String renglon) {
        String SQL = "select idcompra,fecha,nombre,cantidad,precio from compra inner join detallecompra on compra.idcompra = detallecompra.compra_idcompra where compra_idcompra = '?3' and fecha between '?1' and '?2'";
        SQL = SQL.replace("?1", Inicio);
        SQL = SQL.replace("?2", Final);
        SQL = SQL.replace("?3", renglon);
        ResultSet ListaDetallesVentas;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaDetallesVentas = mConexion.ejecutarConsulta(SQL);

        return ListaDetallesVentas;
    }
}
