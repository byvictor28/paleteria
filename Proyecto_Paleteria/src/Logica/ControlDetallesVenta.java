/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import BaseDatos.BDConexion;
import Datos.DetallesVenta;
import Datos.Venta;
import Interfaz.Convertidor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author vi_ma
 */
public class ControlDetallesVenta {
    
    public void agregarDetallesVenta(DetallesVenta mDetallesVenta) throws SQLException {
        String SQL = "insert into detalleventa values"
                + "('?1',(select max(idventa) from venta),'?2','?3');";
        SQL = SQL.replace("?1", String.valueOf(mDetallesVenta.getproducto_idproducto()));
        SQL = SQL.replace("?2", String.valueOf(mDetallesVenta.getprecio()));
        SQL = SQL.replace("?3", String.valueOf(mDetallesVenta.getcantidad()));
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);

    }
      
    
    public ResultSet consultarTodasVentas(String Inicio,String Final) {
        String SQL = "select * from venta "
                + "where fecha between '?1' and '?2'";
        SQL = SQL.replace("?1", Inicio);
        SQL = SQL.replace("?2", Final);
        ResultSet ListaDetallesVentas;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaDetallesVentas = mConexion.ejecutarConsulta(SQL);

        return ListaDetallesVentas;
    }
    
    public ResultSet consultarTodasDetallesVentas(String Inicio,String Final, String renglon) {
        String SQL = "select producto.nombre,producto.descripcion,detalleventa.precio,detalleventa.cantidad\n" +
                        "from venta \n" +
                        "	inner join detalleventa on venta.idventa = detalleventa.venta_idventa\n" +
                        "	inner join producto on detalleventa.Producto_idProducto= producto.idProducto "
                + "where venta.fecha between '?1' and '?2' and venta.idventa = '?3'";
        SQL = SQL.replace("?1", Inicio);
        SQL = SQL.replace("?2", Final);
        SQL = SQL.replace("?3", renglon);
        ResultSet ListaDetallesVentas;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaDetallesVentas = mConexion.ejecutarConsulta(SQL);

        return ListaDetallesVentas;
    }
}
