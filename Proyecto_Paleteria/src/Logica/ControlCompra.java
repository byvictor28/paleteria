/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import BaseDatos.BDConexion;
import Datos.Compra;
import Datos.Venta;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Ryogan
 */
public class ControlCompra {
     public void agregarCompra(Compra mCompra) throws SQLException {
        String SQL = "insert into compra values "
                + "(null,'?1','?2')";
        SQL = SQL.replace("?1", mCompra.getFecha());
        SQL = SQL.replace("?2", String.valueOf(mCompra.getTotal()));
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);
    }
    
    public ResultSet consultarTodasCompras() {
        String SQL = "select compra.idcompra,compra.cantidad,compra.precio";
        ResultSet ListaCompras;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaCompras = mConexion.ejecutarConsulta(SQL);

        return ListaCompras;
    }
}
