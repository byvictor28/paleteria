/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import BaseDatos.BDConexion;
import Datos.Producto;
import Datos.Venta;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author vi_ma
 */
public class ControlVenta {
    public void agregarVenta(Venta mVenta) throws SQLException {
        String SQL = "insert into venta values "
                + "(null,'?1','?2')";
        SQL = SQL.replace("?1", mVenta.getFecha());
        SQL = SQL.replace("?2", String.valueOf(mVenta.getTotal()));
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        mConexion.ejecutarActualizacion(SQL);

    }
    
    public ResultSet consultarTodasVentas() {
        String SQL = "select venta.* , producto.nombre,producto.precio ,producto_has_venta.precio\n" +
                        "from venta \n" +
                        "	inner join producto_has_venta on venta.idventa = producto_has_venta.venta_idventa\n" +
                        "	inner join producto on producto_has_venta.Producto_idProducto= producto.idProducto; ";
        ResultSet ListaVentas;
        BDConexion mConexion = new BDConexion();
        mConexion.conectar();
        ListaVentas = mConexion.ejecutarConsulta(SQL);

        return ListaVentas;
    }
}
