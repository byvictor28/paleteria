/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author vi_ma
 */
public class Producto {
    
    private int IdProducto;
    private String Nombre;
    private String Descripcion;
    private float Precio = 0;
    private String Imagen;
    private int Visible;

   

    
    public void setIdProducto(int idProducto){
        this.IdProducto =  idProducto;
    }
    public int getIdProducto(){
        return IdProducto;
    }
    
    public void setNombre(String Nombre){
        this.Nombre =  Nombre;
    }
    public String getNombre(){
        return Nombre;
    }
    
    public void setDescripcion(String Descripcion){
        this.Descripcion =  Descripcion;
    }
    public String getDescripcion(){
        return Descripcion;
    }
    
    public void setPrecio(float Precio){
        this.Precio =  Precio;
    }
    public float getPrecio(){
        return Precio;
    }
    
    public void setImagen(String Imagen){
        this.Imagen =  Imagen;
    }
    public String getImagen(){
        return Imagen;
    }
    
    public void setVisible(int Visible){
        this.Visible =  Visible;
    }
    public int getVisible(){
        return Visible;
    }
}
