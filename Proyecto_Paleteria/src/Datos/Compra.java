/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author Ryogan
 */

    public class Compra {
    private int IdCompra;
    private String Fecha;
   
    private float Total;
    
    public void setIdCompra(int IdCompra){
        this.IdCompra =  IdCompra;
    }
    public int getIdCompra(){
        return IdCompra;
    }
    
    public void setFecha(String Fecha){
        this.Fecha =  Fecha;
    }
    public String getFecha(){
        return Fecha;
    }
    
    public void setTotal(float Total){
        this.Total =  Total;
    }
    public float getTotal(){
        return Total;
    }
    
}
