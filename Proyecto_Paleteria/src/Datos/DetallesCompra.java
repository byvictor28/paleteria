/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author Ryogan
 */
public class DetallesCompra {
    private int idDetalleCompra;
    private int compra_idcompra;
    private float precio;
    private float cantidad;
     private String Nombre;
    
    public void setidDetalleCompra(int idDetalleCompra){
        this.idDetalleCompra =  idDetalleCompra;
    }
    public int getidDetalleCompra(){
        return idDetalleCompra;
    }
    
    public void setcompra_idcompra(int compra_idcompra){
        this.compra_idcompra =  compra_idcompra;
    }
    public int getcompra_idcompra(){
        return compra_idcompra;
    }
    
    public void setprecio(float precio){
        this.precio =  precio;
    }
    public float getprecio(){
        return precio;
    }
    
    public void setcantidad(float cantidad){
        this.cantidad =  cantidad;
    }
    public float getcantidad(){
        return cantidad;
    }
    public void setNombre(String Nombre){
        this.Nombre =  Nombre;
    }
    public String getNombre(){
        return Nombre;
    }
}
