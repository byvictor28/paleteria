/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author vi_ma
 */
public class DetallesVenta {
    private int producto_idproducto;
    private int venta_idventa;
    private float precio;
    private int cantidad;
    
    public void setproducto_idproducto(int producto_idproducto){
        this.producto_idproducto =  producto_idproducto;
    }
    public int getproducto_idproducto(){
        return producto_idproducto;
    }
    
    public void setventa_idventa(int venta_idventa){
        this.venta_idventa =  venta_idventa;
    }
    public int getventa_idventa(){
        return venta_idventa;
    }
    
    public void setprecio(float precio){
        this.precio =  precio;
    }
    public float getprecio(){
        return precio;
    }
    
    public void setcantidad(int cantidad){
        this.cantidad =  cantidad;
    }
    public int getcantidad(){
        return cantidad;
    }
}
