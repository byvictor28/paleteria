/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author vi_ma
 */
public class Venta {
    private int IdVenta;
    private String Fecha;
    private float Total;
    
    public void setIdVenta(int IdVenta){
        this.IdVenta =  IdVenta;
    }
    public int getIdVenta(){
        return IdVenta;
    }
    
    public void setFecha(String Fecha){
        this.Fecha =  Fecha;
    }
    public String getFecha(){
        return Fecha;
    }
    
    public void setTotal(float Total){
        this.Total =  Total;
    }
    public float getTotal(){
        return Total;
    }
    
}
