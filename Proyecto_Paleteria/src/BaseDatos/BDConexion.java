/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author vi_ma
 */
public class BDConexion {
    private Connection conexion;
    private Statement comando;

    /**
     * Conecta con un Host de MySQL usando la configuración de ConfigBD.java
     *
     * @return Verdadero - si la conexion es correcta Falso si no se pudo
     * conectar con el servidor
     */
    @SuppressWarnings("empty-statement")
    public boolean conectar() {
        String servidor =  "localhost";
        String basedatos =  "paleteria";
        String usuario =  "jefepaleteria";
        String contrasenna =  "jefepaleteria";
        
        try {
            DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
            this.conexion = DriverManager.getConnection("jdbc:mysql://" + servidor + "/" + basedatos,
                    usuario, contrasenna);
            this.comando = conexion.createStatement();
            return true;
        } catch (Exception error) {
            JOptionPane.showMessageDialog(null, "NO SE PUEDE CONECTAR A LA BASE DE DATOS");
            System.exit(0);
            return false;
        }
    }

    /**
     * Este metodo permite ejecutar altas, bajas y cambios sobre los datos y
     * estructuras
     *
     * @param SQL - Es una instrucción SQL DML como INSERT, UPDATE, DELETE o
     * CALL también soporta instrucciones DDL como CREATE, ALTER y DROP
     * @return 0 o más si la instrucción SQL se ejecuto correctamente, -1 en
     * caso de error
     * @throws java.sql.SQLException
     */
    public int ejecutarActualizacion(String SQL) throws SQLException {
        try {
            int FilasAfectadas;
            FilasAfectadas = this.comando.executeUpdate(SQL);
            System.out.println(SQL + " Ejecutada");
            System.out.println("FILAS AFECTADAS" + FilasAfectadas);
            return FilasAfectadas;
        } catch (Exception error) {
            System.out.println("Error " + error.toString());
            return -1;
        }
    }

    /**
     * Este metodo permite ejecutar altas, bajas y cambios sobre los datos y
     * estructuras
     *
     * @param SQL -Es una instrucción SQL de tipo SELECT o SHOW
     * @return Conjunto de datos del resultado, NULO en caso de error
     */
    public ResultSet ejecutarConsulta(String SQL) {
        try {
            ResultSet resultado = this.comando.executeQuery(SQL);
            System.out.println(SQL + " Ejecutada");
            return resultado;
        } catch (Exception error) {
            System.out.println("Error " + error.toString());
            return null;
        }
    }
    
    public ResultSet FiltrarContactos(String txt){
        String SQL = "select idProducto as 'No.',imagen as 'Imagen', nombre as 'Nombre', descripcion as 'Descripcion', precio as 'Precio'"
                + " from producto where visible = 1 AND (idProducto LIKE "
                + "('"+ txt +"%') or nombre LIKE ('"+ txt +"%') or descripcion LIKE ('"+ txt +"%'))";
        ResultSet ListaSemillas;
        conectar();
        ListaSemillas = ejecutarConsulta(SQL);
        return ListaSemillas;
    }
}
