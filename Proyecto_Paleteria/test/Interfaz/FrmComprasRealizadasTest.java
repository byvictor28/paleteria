/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Logica.ControlDetallesCompra;
import java.sql.ResultSet;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class FrmComprasRealizadasTest {
    
    public FrmComprasRealizadasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Fechas method, of class FrmComprasRealizadas.
     */
    @Test
    public void testFechas() {
        System.out.println("Fechas");
        FrmComprasRealizadas instance = new FrmComprasRealizadas();
        instance.Fechas();
    }

    /**
     * Test of calcularTotal method, of class FrmComprasRealizadas.
     */
    @Test
    public void testCalcularTotal() {
        System.out.println("calcularTotal");
        FrmComprasRealizadas instance = new FrmComprasRealizadas();
        float expResult = 3.0F;
        FrmCompras instance2 = new FrmCompras();
        ControlDetallesCompra mControlDetallesCompra = new ControlDetallesCompra();
        ResultSet tabla = mControlDetallesCompra.consultarTodasCompras("2020/04/18","2020/04/19");
        DefaultTableModel modelo = Convertidor.convertir(tabla);
        instance.TDetalles.setModel(modelo);
        float result = instance.calcularTotal();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of main method, of class FrmComprasRealizadas.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        FrmComprasRealizadas.main(args);
        
    }
    
}
