/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Datos.Producto;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class FrmModificarProductoTest {
    
    public FrmModificarProductoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of cargarImagen method, of class FrmModificarProducto.
     */
    @Test
    public void testCargarImagen() {
        System.out.println("cargarImagen");
        FrmModificarProducto instance = new FrmModificarProducto();
        instance.cargarImagen();
        
    }

    /**
     * Test of llenarcampos method, of class FrmModificarProducto.
     */
    @Test
    public void testLlenarcampos() {
        System.out.println("llenarcampos");
        Producto mProducto = new Producto();
        mProducto.setNombre("Nacho");
        mProducto.setDescripcion("aklsd");
        mProducto.setImagen("D:\\Documentos\\Paletalimon.jpg");
        mProducto.setPrecio(10);
        mProducto.setVisible(1);
        FrmModificarProducto instance = new FrmModificarProducto();
        instance.llenarcampos(mProducto);
       
    }

    /**
     * Test of main method, of class FrmModificarProducto.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        FrmModificarProducto.main(args);
    
    }
    
}
