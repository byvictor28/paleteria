/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class FrmComprasTest {
    
    public FrmComprasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of FormatoTabla method, of class FrmCompras.
     */
    @Test
    public void testFormatoTabla() {
        System.out.println("FormatoTabla");
        FrmCompras instance = new FrmCompras();
        instance.FormatoTabla();
       
    }

    /**
     * Test of contar method, of class FrmCompras.
     */
    @Test
    public void testContar() {
        System.out.println("contar");
        FrmCompras instance = new FrmCompras();
        int expResult = 1;
        int result = instance.contar();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of restar method, of class FrmCompras.
     */
    @Test
    public void testRestar() {
        System.out.println("restar");
        FrmCompras instance = new FrmCompras();
        int expResult = 1;
        int result = instance.restar();
        assertEquals(expResult, result);
    
    }

    /**
     * Test of calcularTotal method, of class FrmCompras.
     */
    @Test
    public void testCalcularTotal() {
        System.out.println("calcularTotal");
        FrmCompras instance = new FrmCompras();
        float expResult = 0.0F;
        float result = instance.calcularTotal();
        assertEquals(expResult, result, 0.0);
     
    }

    /**
     * Test of main method, of class FrmCompras.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        FrmCompras.main(args);
        
    }
    
}
