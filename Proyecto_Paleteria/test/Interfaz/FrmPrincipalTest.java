/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class FrmPrincipalTest {
    
    public FrmPrincipalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of cargarImagen method, of class FrmPrincipal.
     */
    @Test
    public void testCargarImagen() {
        System.out.println("cargarImagen");
        FrmPrincipal instance = new FrmPrincipal();
        instance.cargarImagen();
    }

    /**
     * Test of FormatoTabla method, of class FrmPrincipal.
     */
    @Test
    public void testFormatoTabla() {
        System.out.println("FormatoTabla");
        FrmPrincipal instance = new FrmPrincipal();
        instance.FormatoTabla();
        
    }

    /**
     * Test of contar method, of class FrmPrincipal.
     */
    @Test
    public void testContar() {
        System.out.println("contar");
        FrmPrincipal instance = new FrmPrincipal();
        int expResult = 1;
        int result = instance.contar();
        assertEquals(expResult, result,0);
    }

    /**
     * Test of restar method, of class FrmPrincipal.
     */
    @Test
    public void testRestar() {
    
        FrmPrincipal instance = new FrmPrincipal();
        int expResult = 1;
        int result = instance.restar();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
       
    }

    /**
     * Test of calcularTotal method, of class FrmPrincipal.
     */
    @Test
    public void testCalcularTotal() {
        System.out.println("calcularTotal");
        FrmPrincipal instance = new FrmPrincipal();
        float expResult = 0.0F;
        float result = instance.calcularTotal();
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of Limpiar method, of class FrmPrincipal.
     */
    @Test
    public void testLimpiar() {
        System.out.println("Limpiar");
        FrmPrincipal instance = new FrmPrincipal();
        instance.Limpiar();
       
    }

    /**
     * Test of poputTable method, of class FrmPrincipal.
     */
    @Test
    public void testPoputTable() {
        System.out.println("poputTable");
        FrmPrincipal instance = new FrmPrincipal();
        instance.poputTable();
       
    }

    /**
     * Test of poputTable2 method, of class FrmPrincipal.
     */
    @Test
    public void testPoputTable2() {
        System.out.println("poputTable2");
        FrmPrincipal instance = new FrmPrincipal();
        instance.poputTable2();
   
    }

    /**
     * Test of main method, of class FrmPrincipal.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        FrmPrincipal.main(args);
      
    }
    
}
