/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDatos;

import Interfaz.Convertidor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class BDConexionTest {
    
    public BDConexionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of conectar method, of class BDConexion.
     */
    @Test
    public void testConectar() {
        System.out.println("conectar");
        BDConexion instance = new BDConexion();
        boolean expResult = true;
        boolean result = instance.conectar();
        assertEquals(expResult, result);
    }

    /**
     * Test of ejecutarActualizacion method, of class BDConexion.
     */
    @Test
    public void testEjecutarActualizacion() throws Exception {
        System.out.println("ejecutarActualizacion");
        String SQL = "update producto set nombre='Paleta',descripcion='Limon',precio='13.0',imagen = 'D:\\Documentos\\Paletalimon.jpg',visible = '1' where idproducto = '3'";
        BDConexion instance = new BDConexion();
        instance.conectar();
        int expResult = 1;
        int result = instance.ejecutarActualizacion(SQL);
        assertEquals(expResult, result);
    }

    /**
     * Test of ejecutarConsulta method, of class BDConexion.
     */
    @Test
    public void testEjecutarConsulta() throws SQLException {
        System.out.println("ejecutarConsulta");
        boolean expResult = true;
        String id = "3";
        String SQL = "select producto.descripcion from producto where idproducto = ('"+ id +"%')";
        BDConexion instance = new BDConexion();
        instance.conectar();
        ResultSet result = instance.ejecutarConsulta(SQL);
         boolean resultado = false;
        if (result.absolute(1)){
            System.out.println("Hay mas de una fila");
           resultado = true;
        }else{
           resultado = false;
        }
   
        assertEquals(expResult, resultado);
    }

    /**
     * Test of FiltrarContactos method, of class BDConexion.
     */
    @Test
    public void testFiltrarContactos() {
        System.out.println("FiltrarContactos");
        String idproducto = "3";
        String nombre = "Paleta";
        String descripcion = "Limon";
        String Imagen = "D:\\Documentos\\Paletalimon.jpg";
        String precio = "12.0";
        String txt = "3";
        BDConexion instance = new BDConexion();
        ResultSet result = instance.FiltrarContactos(txt);
         DefaultTableModel modelo = Convertidor.convertir(result);
       String idproducto2 = modelo.getValueAt(0, 0).toString();
       String Imagen2 = modelo.getValueAt(0, 1).toString();
       String Nombre2 = modelo.getValueAt(0, 2).toString();
       String Descripcion2 = modelo.getValueAt(0, 3).toString();
       String Precio2 = modelo.getValueAt(0, 4).toString();
        assertEquals(idproducto, idproducto2);
        assertEquals(nombre, Nombre2);
        assertEquals(descripcion, Descripcion2);
        assertEquals(Imagen, Imagen2);
        assertEquals(precio, Precio2);
    }
    
}
