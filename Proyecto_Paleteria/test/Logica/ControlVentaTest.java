/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.Venta;
import java.sql.ResultSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class ControlVentaTest {
    
    public ControlVentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarVenta method, of class ControlVenta.
     */
    @Test
    public void testAgregarVenta() throws Exception {
        System.out.println("agregarVenta");
        Venta mVenta = new Venta();
        mVenta.setFecha("20/10/2020");
        mVenta.setIdVenta(1);
        mVenta.setTotal(100);
        ControlVenta instance = new ControlVenta();
        instance.agregarVenta(mVenta);
    }

    /**
     * Test of consultarTodasVentas method, of class ControlVenta.
     */
    @Test
    public void testConsultarTodasVentas() {
        System.out.println("consultarTodasVentas");
        ControlVenta instance = new ControlVenta();
        ResultSet expResult = null;
        ResultSet result = instance.consultarTodasVentas();
        assertEquals(expResult, result);
    }
    
}
