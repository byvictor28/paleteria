/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.DetallesCompra;
import Interfaz.Convertidor;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class ControlDetallesCompraTest {
    
    public ControlDetallesCompraTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarDetallesCompra method, of class ControlDetallesCompra.
     */
    @Test
    public void testAgregarDetallesCompra() throws Exception {
        System.out.println("agregarDetallesCompra");
        DetallesCompra mDetallesCompra = new DetallesCompra();
        mDetallesCompra.setcantidad(1);
        mDetallesCompra.setprecio(10);
        ControlDetallesCompra instance = new ControlDetallesCompra();
        instance.agregarDetallesCompra(mDetallesCompra);
    }

    /**
     * Test of consultarTodasCompras method, of class ControlDetallesCompra.
     */
    @Test
    public void testConsultarTodasCompras() {
          System.out.println("consultarTodasCompras");
        String Inicio = "2020/04/18";
        String Final = "2020/04/19";
        ControlDetallesCompra instance = new ControlDetallesCompra();
        String idcompra = "4";
        String fecha= "2020-04-19";
        String total = "3.0";
        ResultSet result = instance.consultarTodasCompras(Inicio, Final);
        DefaultTableModel modelo = Convertidor.convertir(result);
       String idcompra2 = modelo.getValueAt(0, 0).toString();
       String fecha2 = modelo.getValueAt(0,1).toString();
       String total2 = modelo.getValueAt(0,2).toString();
         assertEquals(idcompra, idcompra2);
          assertEquals(fecha, fecha2);
          assertEquals(total, total2);
    }

    /**
     * Test of consultarTodasDetallesCompras method, of class ControlDetallesCompra.
     */
    @Test
    public void testConsultarTodasDetallesCompras() {
        System.out.println("consultarTodasDetallesCompras");
        String renglon = "4";
        String Inicio = "2020/04/18";
        String Final = "2020/04/19";
        ControlDetallesCompra instance = new ControlDetallesCompra();
        String idcompra = "4";
        String fecha = "2020-04-19";
        String cantidad = "1";
        String Precio = "3.0";
        ResultSet result = instance.consultarTodasDetallesCompras(Inicio, Final, renglon);
        DefaultTableModel modelo = Convertidor.convertir(result);
       String idcompra2= modelo.getValueAt(0, 0).toString();
       String fecha2 = modelo.getValueAt(0,1).toString();
       String cantidad2 = modelo.getValueAt(0,2).toString();
       String Precio2 = modelo.getValueAt(0,3).toString();
         assertEquals(idcompra, idcompra2);
          assertEquals(fecha, fecha2);
          assertEquals(cantidad, cantidad2);
          assertEquals(Precio, Precio2);
    }
    
}
