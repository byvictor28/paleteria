/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.DetallesVenta;
import Interfaz.Convertidor;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class ControlDetallesVentaTest {
    
    public ControlDetallesVentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarDetallesVenta method, of class ControlDetallesVenta.
     */
    @Test
    public void testAgregarDetallesVenta() throws Exception {
        System.out.println("agregarDetallesVenta");
        String Descripcion = "Limon";
        DetallesVenta mDetallesVenta = new DetallesVenta();
        mDetallesVenta.setcantidad(10);
        mDetallesVenta.setprecio(10);
        ControlDetallesVenta instance = new ControlDetallesVenta();
        instance.agregarDetallesVenta( mDetallesVenta);
     
    }

    /**
     * Test of consultarTodasVentas method, of class ControlDetallesVenta.
     */
    @Test
    public void testConsultarTodasVentas() {
       System.out.println("consultarTodasVentas");
        String Inicio = "2020/04/18";
        String Final = "2020/04/19";
        ControlDetallesVenta instance = new ControlDetallesVenta();
        String idventa = "2";
        String fecha= "2020-04-19";
        String total = "24.0";
        ResultSet result = instance.consultarTodasVentas(Inicio, Final);
        DefaultTableModel modelo = Convertidor.convertir(result);
       String idventa2 = modelo.getValueAt(0, 0).toString();
       String fecha2 = modelo.getValueAt(0,1).toString();
       String total2 = modelo.getValueAt(0,2).toString();
         assertEquals(idventa, idventa2);
          assertEquals(fecha, fecha2);
          assertEquals(total, total2);
    }

    /**
     * Test of consultarTodasDetallesVentas method, of class ControlDetallesVenta.
     */
    @Test
    public void testConsultarTodasDetallesVentas() {
     System.out.println("consultarTodasDetallesVentas");
        String renglon = "2";
        String Inicio = "2020/04/18";
        String Final = "2020/04/19";
        ControlDetallesVenta instance = new ControlDetallesVenta();
        String Nombre = "Paleta";
        String Descripcion= "Limon";
        ResultSet result = instance.consultarTodasDetallesVentas(Inicio, Final, renglon);
        DefaultTableModel modelo = Convertidor.convertir(result);
       String Nombre2 = modelo.getValueAt(0, 0).toString();
       String Descripcion2 = modelo.getValueAt(0,1).toString();
         assertEquals(Nombre, Nombre2);
          assertEquals(Descripcion, Descripcion2);
    }
    
}
