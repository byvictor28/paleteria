/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.Producto;
import Interfaz.Convertidor;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class ControlProductoTest {
    
    public ControlProductoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarProducto method, of class ControlProducto.
     */
    @Test
    public void testAgregarProducto() throws Exception {
        System.out.println("agregarProducto");
        Producto mProducto = new Producto();
        mProducto.setIdProducto(123);
        mProducto.setNombre("Banana");
        mProducto.setDescripcion("asldkasl54d");
        mProducto.setImagen("slskdalsd");
        mProducto.setPrecio(10);
        mProducto.setVisible(1);
        ControlProducto instance = new ControlProducto();
        instance.agregarProducto(mProducto);
    }

    /**
     * Test of eliminarProducto method, of class ControlProducto.
     */
    @Test
    public void testEliminarProducto() throws Exception {      
        System.out.println("eliminarProducto");
        String id = "1";
        ControlProducto instance = new ControlProducto();
        instance.eliminarProducto(id);
    }

    /**
     * Test of modificarProducto method, of class ControlProducto.
     */
    @Test
    public void testModificarProducto() throws Exception {
        System.out.println("modificarProducto");
        Producto mProducto = new Producto();
        mProducto.setIdProducto(1);
        mProducto.setNombre("Banana");
        mProducto.setDescripcion("xdakl");
        mProducto.setImagen("slskdalsd");
        mProducto.setPrecio(10);
        mProducto.setVisible(1);
        String row = "0";
        ControlProducto instance = new ControlProducto();
        instance.modificarProducto(mProducto, row);
    }

    /**
     * Test of ObtenerUrl method, of class ControlProducto.
     */
    @Test
    public void testObtenerUrl() throws Exception {
      System.out.println("ObtenerUrl");
        ControlProducto instance = new ControlProducto();
        String row = "3";
        String url = "D:\\Documentos\\Paletalimon.jpg";
        ResultSet result = instance.ObtenerUrl(row);
         String[] arr = null;
        while (result.next()) {
        String em = result.getString("imagen");
        arr = em.split("\n");
        for (int i =0; i < arr.length; i++){
         System.out.println(arr[i]);
        }
    }
       String url2 = arr[0];
        System.out.println("RESULTADO URL" +url2);
        assertEquals(url, url2);
    }

    /**
     * Test of consultarTodasProductos method, of class ControlProducto.
     */
    @Test
    public void testConsultarTodasProductos() {
        System.out.println("consultarTodasProductos");
        ControlProducto instance = new ControlProducto();
        Convertidor mConvertidor = new Convertidor();
        String idproducto = "3";
        String Nombre = "Paleta";
        ResultSet result = instance.consultarTodasProductos();
       DefaultTableModel modelo = Convertidor.convertir(result);
       String idproducto2 = modelo.getValueAt(0, 0).toString();
       String nombre2 = modelo.getValueAt(0, 2).toString();
        assertEquals(idproducto, idproducto2);
        assertEquals(Nombre,nombre2);
    }
    
}
