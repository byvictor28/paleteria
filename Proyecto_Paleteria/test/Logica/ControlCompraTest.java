/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.Compra;
import java.sql.ResultSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class ControlCompraTest {
    
    public ControlCompraTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of agregarCompra method, of class ControlCompra.
     */
    @Test
    public void testAgregarCompra() throws Exception {
        System.out.println("agregarCompra");
        Compra mCompra = new Compra();
        mCompra.setFecha("20/20/2020");
        mCompra.setIdCompra(1);
        mCompra.setTotal(15);
        ControlCompra instance = new ControlCompra();
        instance.agregarCompra(mCompra);
    }

    /**
     * Test of consultarTodasCompras method, of class ControlCompra.
     */
    @Test
    public void testConsultarTodasCompras() {
        System.out.println("consultarTodasCompras");
        ControlCompra instance = new ControlCompra();
        ResultSet expResult = null;
        ResultSet result = instance.consultarTodasCompras();
        assertEquals(expResult, result);
    }
    
}
