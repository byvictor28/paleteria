/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class ProductoTest {
    
    public ProductoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setIdProducto method, of class Producto.
     */
    @Test
    public void testSetIdProducto() {
        System.out.println("setIdProducto");
        int idProducto = 100;
        Producto instance = new Producto();
        instance.setIdProducto(idProducto);
    }

    /**
     * Test of getIdProducto method, of class Producto.
     */
    @Test
    public void testGetIdProducto() {
        System.out.println("getIdProducto");
        Producto instance = new Producto();
        instance.setIdProducto(100);
        int expResult = 100;
        int result = instance.getIdProducto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNombre method, of class Producto.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String Nombre = "Doritos";
        Producto instance = new Producto();
        instance.setNombre(Nombre);
    }

    /**
     * Test of getNombre method, of class Producto.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        Producto instance = new Producto();
        instance.setNombre("Doritos");
        String expResult = "Doritos";
        String result = instance.getNombre();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescripcion method, of class Producto.
     */
    @Test
    public void testSetDescripcion() {
        System.out.println("setDescripcion");
        String Descripcion = "OnMyOwn";
        Producto instance = new Producto();
        instance.setDescripcion(Descripcion);
    }

    /**
     * Test of getDescripcion method, of class Producto.
     */
    @Test
    public void testGetDescripcion() {
        System.out.println("getDescripcion");
        Producto instance = new Producto();
        instance.setDescripcion("OnMyOwn");
        String expResult = "OnMyOwn";
        String result = instance.getDescripcion();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPrecio method, of class Producto.
     */
    @Test
    public void testSetPrecio() {
        System.out.println("setPrecio");
        float Precio = 14.0F;
        Producto instance = new Producto();
        instance.setPrecio(Precio);
    }

    /**
     * Test of getPrecio method, of class Producto.
     */
    @Test
    public void testGetPrecio() {
        System.out.println("getPrecio");
        Producto instance = new Producto();
        instance.setPrecio(14.0F);
        float expResult = 14.0F;
        float result = instance.getPrecio();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setImagen method, of class Producto.
     */
    @Test
    public void testSetImagen() {
        System.out.println("setImagen");
        String Imagen = "URLLL";
        Producto instance = new Producto();
        instance.setImagen(Imagen);
    }

    /**
     * Test of getImagen method, of class Producto.
     */
    @Test
    public void testGetImagen() {
        System.out.println("getImagen");
        Producto instance = new Producto();
        instance.setImagen("URLLL");
        String expResult = "URLLL";
        String result = instance.getImagen();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVisible method, of class Producto.
     */
    @Test
    public void testSetVisible() {
        System.out.println("setVisible");
        int Visible = 1;
        Producto instance = new Producto();
        instance.setVisible(Visible);
    }

    /**
     * Test of getVisible method, of class Producto.
     */
    @Test
    public void testGetVisible() {
        System.out.println("getVisible");
        Producto instance = new Producto();
        instance.setVisible(1);
        int expResult = 1;
        int result = instance.getVisible();
        assertEquals(expResult, result);
    }
    
}
