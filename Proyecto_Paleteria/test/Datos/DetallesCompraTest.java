/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class DetallesCompraTest {
    
    public DetallesCompraTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setproducto_idproducto method, of class DetallesCompra.
     */
    @Test
    public void testSetproducto_idproducto() {
        System.out.println("setproducto_idproducto");
        int producto_idproducto = 200;
        DetallesCompra instance = new DetallesCompra();
        instance.setidDetalleCompra(producto_idproducto);
    }

    /**
     * Test of getproducto_idproducto method, of class DetallesCompra.
     */
    @Test
    public void testGetproducto_idproducto() {
        System.out.println("getproducto_idproducto");
        DetallesCompra instance = new DetallesCompra();
        instance.setidDetalleCompra(200);
        int expResult = 200;
        int result = instance.getidDetalleCompra();
        assertEquals(expResult, result);
    }

    /**
     * Test of setcompra_idcompra method, of class DetallesCompra.
     */
    @Test
    public void testSetcompra_idcompra() {
        System.out.println("setcompra_idcompra");
        int compra_idcompra = 700;
        DetallesCompra instance = new DetallesCompra();
        instance.setcompra_idcompra(compra_idcompra);
    }

    /**
     * Test of getcompra_idcompra method, of class DetallesCompra.
     */
    @Test
    public void testGetcompra_idcompra() {
        System.out.println("getcompra_idcompra");
        DetallesCompra instance = new DetallesCompra();
        instance.setcompra_idcompra(700);
        int expResult = 700;
        int result = instance.getcompra_idcompra();
        assertEquals(expResult, result);
    }

    /**
     * Test of setprecio method, of class DetallesCompra.
     */
    @Test
    public void testSetprecio() {
        System.out.println("setprecio");
        float precio = 80.4F;
        DetallesCompra instance = new DetallesCompra();
        instance.setprecio(precio);
    }

    /**
     * Test of getprecio method, of class DetallesCompra.
     */
    @Test
    public void testGetprecio() {
        System.out.println("getprecio");
        DetallesCompra instance = new DetallesCompra();
        instance.setprecio(80.4F);
        float expResult = 80.4F;
        float result = instance.getprecio();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setcantidad method, of class DetallesCompra.
     */
    @Test
    public void testSetcantidad() {
        System.out.println("setcantidad");
        float cantidad = 6;
        DetallesCompra instance = new DetallesCompra();
        instance.setcantidad(cantidad);
    }

    /**
     * Test of getcantidad method, of class DetallesCompra.
     */
    @Test
    public void testGetcantidad() {
        System.out.println("getcantidad");
        DetallesCompra instance = new DetallesCompra();
        instance.setcantidad(6.0F);
        float expResult = 6.0F;
        float result = instance.getcantidad();
        assertEquals(expResult, result,0.0);
    }
    
}
