/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class VentaTest {
    
    public VentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setIdVenta method, of class Venta.
     */
    @Test
    public void testSetIdVenta() {
        System.out.println("setIdVenta");
        int IdVenta = 10;
        Venta instance = new Venta();
        instance.setIdVenta(IdVenta);
    }

    /**
     * Test of getIdVenta method, of class Venta.
     */
    @Test
    public void testGetIdVenta() {
        System.out.println("getIdVenta");
        Venta instance = new Venta();
        instance.setIdVenta(10);
        int expResult = 10;
        int result = instance.getIdVenta();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha method, of class Venta.
     */
    @Test
    public void testSetFecha() {
        System.out.println("setFecha");
        String Fecha = "20/04/2020";
        Venta instance = new Venta();
        instance.setFecha(Fecha);
    }

    /**
     * Test of getFecha method, of class Venta.
     */
    @Test
    public void testGetFecha() {
        System.out.println("getFecha");
        Venta instance = new Venta();
        instance.setFecha("20/04/2020");
        String expResult = "20/04/2020";
        String result = instance.getFecha();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTotal method, of class Venta.
     */
    @Test
    public void testSetTotal() {
        System.out.println("setTotal");
        float Total = 15.4F;
        Venta instance = new Venta();
        instance.setTotal(Total);
    }

    /**
     * Test of getTotal method, of class Venta.
     */
    @Test
    public void testGetTotal() {
        System.out.println("getTotal");
        Venta instance = new Venta();
        instance.setTotal(15.4F);
        float expResult = 15.4F;
        float result = instance.getTotal();
        assertEquals(expResult, result, 0.0);
    }
    
}
