/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class CompraTest {
    
    public CompraTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setIdCompra method, of class Compra.
     */
    @Test
    public void testSetIdCompra() {
        System.out.println("setIdCompra");
        int IdCompra = 400;
        Compra instance = new Compra();
        instance.setIdCompra(IdCompra);
    }

    /**
     * Test of getIdCompra method, of class Compra.
     */
    @Test
    public void testGetIdCompra() {
        System.out.println("getIdCompra");
        Compra instance = new Compra();
        instance.setIdCompra(400);
        int expResult = 400;
        int result = instance.getIdCompra();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFecha method, of class Compra.
     */
    @Test
    public void testSetFecha() {
        System.out.println("setFecha");
        String Fecha = "20/04/2020";
        Compra instance = new Compra();
        instance.setFecha(Fecha);
    }

    /**
     * Test of getFecha method, of class Compra.
     */
    @Test
    public void testGetFecha() {
        System.out.println("getFecha");
        Compra instance = new Compra();
        instance.setFecha("20/04/2020");
        String expResult = "20/04/2020";
        String result = instance.getFecha();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTotal method, of class Compra.
     */
    @Test
    public void testSetTotal() {
        System.out.println("setTotal");
        float Total = 100.0F;
        Compra instance = new Compra();
        instance.setTotal(Total);
    }

    /**
     * Test of getTotal method, of class Compra.
     */
    @Test
    public void testGetTotal() {
        System.out.println("getTotal");
        Compra instance = new Compra();
        instance.setTotal(100.0F);
        float expResult = 100.0F;
        float result = instance.getTotal();
        assertEquals(expResult, result, 0.0);
    }
    
}
