/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ryogan
 */
public class DetallesVentaTest {
    
    public DetallesVentaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setproducto_idproducto method, of class DetallesVenta.
     */
    @Test
    public void testSetproducto_idproducto() {
        System.out.println("setproducto_idproducto");
        int producto_idproducto = 100;
        DetallesVenta instance = new DetallesVenta();
        instance.setproducto_idproducto(producto_idproducto);
    }

    /**
     * Test of getproducto_idproducto method, of class DetallesVenta.
     */
    @Test
    public void testGetproducto_idproducto() {
        System.out.println("getproducto_idproducto");
        DetallesVenta instance = new DetallesVenta();
        instance.setproducto_idproducto(100);
        int expResult = 100;
        int result = instance.getproducto_idproducto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setventa_idventa method, of class DetallesVenta.
     */
    @Test
    public void testSetventa_idventa() {
        System.out.println("setventa_idventa");
        int venta_idventa = 100;
        DetallesVenta instance = new DetallesVenta();
        instance.setventa_idventa(venta_idventa);
    }

    /**
     * Test of getventa_idventa method, of class DetallesVenta.
     */
    @Test
    public void testGetventa_idventa() {
        System.out.println("getventa_idventa");
        DetallesVenta instance = new DetallesVenta();
        instance.setventa_idventa(100);
        int expResult = 100;
        int result = instance.getventa_idventa();
        assertEquals(expResult, result);
    }

    /**
     * Test of setprecio method, of class DetallesVenta.
     */
    @Test
    public void testSetprecio() {
        System.out.println("setprecio");
        float precio = 16.5F;
        DetallesVenta instance = new DetallesVenta();
        instance.setprecio(precio);
    }

    /**
     * Test of getprecio method, of class DetallesVenta.
     */
    @Test
    public void testGetprecio() {
        System.out.println("getprecio");
        DetallesVenta instance = new DetallesVenta();
        instance.setprecio(16.5F);
        float expResult = 16.5F;
        float result = instance.getprecio();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setcantidad method, of class DetallesVenta.
     */
    @Test
    public void testSetcantidad() {
        System.out.println("setcantidad");
        int cantidad = 3;
        DetallesVenta instance = new DetallesVenta();
        instance.setcantidad(cantidad);
    }

    /**
     * Test of getcantidad method, of class DetallesVenta.
     */
    @Test
    public void testGetcantidad() {
        System.out.println("getcantidad");
        DetallesVenta instance = new DetallesVenta();
        instance.setcantidad(3);
        int expResult = 3;
        int result = instance.getcantidad();
        assertEquals(expResult, result);
    }
    
}
